﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {
        Graphics g, g1, g2, g3, g4, g5, g6, g7, g8, g9;
        List<Pen> pens = new List<Pen>();
        
        public Form1()
        {
            InitializeComponent();
            
            g1 = pictureBox1.CreateGraphics();
            g2 = pictureBox2.CreateGraphics();
            g3 = pictureBox3.CreateGraphics();
            g4 = pictureBox4.CreateGraphics();
            g5 = pictureBox5.CreateGraphics();
            g6 = pictureBox6.CreateGraphics();
            g7 = pictureBox7.CreateGraphics();
            g8 = pictureBox8.CreateGraphics();
            g9 = pictureBox9.CreateGraphics();
            pens.Add(new Pen(Color.Black, 2F));

            if (popunjen[2] == 1)
                MessageBox.Show("da");
        }

        int igrac = 0;
        int [] popunjen = new int[] { 0,0,0,0,0,0,0,0,0};
        
                
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (popunjen[0] == 0) { 
                if (igrac == 0)
                {
                    Circle c = new Circle();
                    c.draw(g1, pens[0], 20, 10);
                    igrac = 1;
                }
                else
                {
                    Linija l = new Linija();
                    l.draw(g1, pens[0], 30, 10, 90, 80);
                    l.draw(g1, pens[0], 90, 10, 30, 80);
                    igrac = 0;
                }
                popunjen[0] = 1;
            }
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            if (popunjen[1] == 0)
            {
                if (igrac == 0)
                {
                    Circle c = new Circle();
                    c.draw(g2, pens[0], 20, 10);
                    igrac = 1;
                }
                else
                {
                    Linija l = new Linija();
                    l.draw(g2, pens[0], 30, 10, 90, 80);
                    l.draw(g2, pens[0], 90, 10, 30, 80);
                    igrac = 0;
                }
                popunjen[1] = 1;
            }
        }
        private void pictureBox3_MouseUp(object sender, MouseEventArgs e)
        {
            if (popunjen[2] == 0)
            {
                if (igrac == 0)
                {
                    Circle c = new Circle();
                    c.draw(g3, pens[0], 20, 10);
                    igrac = 1;
                }
                else
                {
                    Linija l = new Linija();
                    l.draw(g3, pens[0], 30, 10, 90, 80);
                    l.draw(g3, pens[0], 90, 10, 30, 80);
                    igrac = 0;
                }
                popunjen[2] = 1;
            }
        }
        private void pictureBox4_MouseUp(object sender, MouseEventArgs e)
        {
            if (popunjen[3] == 0)
            {
                if (igrac == 0)
                {
                    Circle c = new Circle();
                    c.draw(g4, pens[0], 20, 10);
                    igrac = 1;
                }
                else
                {
                    Linija l = new Linija();
                    l.draw(g4, pens[0], 30, 10, 90, 80);
                    l.draw(g4, pens[0], 90, 10, 30, 80);
                    igrac = 0;
                }
                popunjen[3] = 1;
            }
        }
        private void pictureBox5_MouseUp(object sender, MouseEventArgs e)
        {
            if (popunjen[4] == 0)
            {
                if (igrac == 0)
                {
                    Circle c = new Circle();
                    c.draw(g5, pens[0], 20, 10);
                    igrac = 1;
                }
                else
                {
                    Linija l = new Linija();
                    l.draw(g5, pens[0], 30, 10, 90, 80);
                    l.draw(g5, pens[0], 90, 10, 30, 80);
                    igrac = 0;
                }
                popunjen[4] = 1;
            }
        }
        private void pictureBox6_MouseUp(object sender, MouseEventArgs e)
        {
            if (popunjen[5] == 0)
            {
                if (igrac == 0)
                {
                    Circle c = new Circle();
                    c.draw(g6, pens[0], 20, 10);
                    igrac = 1;
                }
                else
                {
                    Linija l = new Linija();
                    l.draw(g6, pens[0], 30, 10, 90, 80);
                    l.draw(g6, pens[0], 90, 10, 30, 80);
                    igrac = 0;
                }
                popunjen[5] = 1;
            }
        }
        private void pictureBox7_MouseUp(object sender, MouseEventArgs e)
        {
            if (popunjen[6] == 0)
            {
                if (igrac == 0)
                {
                    Circle c = new Circle();
                    c.draw(g7, pens[0], 20, 10);
                    igrac = 1;
                }
                else
                {
                    Linija l = new Linija();
                    l.draw(g7, pens[0], 30, 10, 90, 80);
                    l.draw(g7, pens[0], 90, 10, 30, 80);
                    igrac = 0;
                }
                popunjen[6] = 1;
            }
         }
        private void pictureBox8_MouseUp(object sender, MouseEventArgs e)
        {
            if (popunjen[7] == 0)
            {
                if (igrac == 0)
                {
                    Circle c = new Circle();
                    c.draw(g8, pens[0], 20, 10);
                    igrac = 1;
                }
                else
                {
                    Linija l = new Linija();
                    l.draw(g8, pens[0], 30, 10, 90, 80);
                    l.draw(g8, pens[0], 90, 10, 30, 80);
                    igrac = 0;
                }
                popunjen[7] = 1;
            }
        }
        private void pictureBox9_MouseUp(object sender, MouseEventArgs e)
        {
            if (popunjen[8] == 0)
            {
                if (igrac == 0)
                {
                    Circle c = new Circle();
                    c.draw(g9, pens[0], 20, 10);
                    igrac = 1;
                }
                else
                {
                    Linija l = new Linija();
                    l.draw(g9, pens[0], 30, 10, 90, 80);
                    l.draw(g9, pens[0], 90, 10, 30, 80);
                    igrac = 0;
                }
                popunjen[8] = 1;
            }
        }
    }

    

    //KLASE ZA CRTANJE
    class Circle
    {
        int r;
        public Circle() { r = 80; }
        public void draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawEllipse(p, x, y, r, r);
        }
    }
    class Linija
    {
        public void draw(Graphics g, Pen p, int x, int y, int w, int z)
        {
            g.DrawLine(p, x, y, w, z);
        }
    }
}
